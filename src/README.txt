CSCI3170 Project Phase 2 - README File
------------------------------
Group Number: 7
Group Members:
1. Fan Sin Tat (1155047763)
2. Lee Ho Ming  (1155031881)
3. Javier Rodriguez (1155080501)
------------------------------
List of Source Files:

1. MainApplication.java
This file is the entry point of the whole system. It will setup the database connection,
and then call MenuManager for action(s) to perform.

2. MenuManager.java
This file maintains the main flow of the system. It handles the menu and user's selection.

3. DatabasesManager.java
This file holds the database connection instance, and also provides some handy functions
to deal with database query or result set.

4. AdministratorAction.java
5. LibraryUserAction.java
6. LibrarianAction.java
7. DirectorAction.java
As explained by their own name, these four files perform actual actions by those four roles.
------------------------------
Methods of compilation and execution:
Compile: javac MainApplication.java
Execute: java -classpath ./ojdbc6.jar:./ MainApplication
