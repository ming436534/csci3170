import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Po on 3/4/2016.
 */
public class LibraryUserAction {
    LibraryUserAction(){

    }
    public void searchBooks() {
        //display
        System.out.println("");
        System.out.println("Choose the search criterion");
        System.out.println("1. call number");
        System.out.println("2. title");
        System.out.println("3. author");
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Choose the search criterion:");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){

        }
        String choice2;
        switch(choice){
            case 1:
                System.out.println("");
                System.out.println("Please type in the call number");
                keyboard = new Scanner(System.in);
                choice2 = keyboard.nextLine();
                searchByCallNum(choice2, true);
                break;
            case 2:
                System.out.println("");
                System.out.println("Please type in the title");
                keyboard = new Scanner(System.in);
                choice2 = keyboard.nextLine();
                searchByTitle(choice2);
                break;
            case 3:
                System.out.println("");
                System.out.println("Please type in the name of the author");
                keyboard = new Scanner(System.in);
                choice2 = keyboard.nextLine();
                searchByAuthor(choice2);
                break;
            default:
                System.out.println("");
                System.out.println("Invalid input, please enter number 1, 2 or 3");
        }
    }



    public void searchByCallNum(String inputCallNum, boolean printFirstLine){
        Connection conn = DatabaseManager.getConnection();
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM book, authorship where book.callnum = authorship.callnum and book.callnum = ?");
            preparedStatement.setString(1, inputCallNum);

            ResultSet rs = preparedStatement.executeQuery();

            String callNum, title, author;
            int borrowedCopy, totalNumOfCopy;
            if(rs.next()){
                callNum = rs.getString("callnum");
                title = rs.getString("title");

                // Author name
                author = rs.getString("aname");
                while(rs.next()) author += ", "+ rs.getString("aname");

                // Number of copies
                preparedStatement = conn.prepareStatement("SELECT count(*) as numOfCopy from bookcopy where callnum = ?");
                preparedStatement.setString(1, inputCallNum);
                rs = preparedStatement.executeQuery();
                rs.next();
                totalNumOfCopy = rs.getInt("numOfCopy");

                // Borrow Record
                preparedStatement = conn.prepareStatement("SELECT count(*) as borrowed from Borrow where callnum = ? AND returndate IS NULL");
                preparedStatement.setString(1, inputCallNum);
                rs = preparedStatement.executeQuery();
                rs.next();
                borrowedCopy = rs.getInt("borrowed");
                int available = totalNumOfCopy - borrowedCopy;

                if (printFirstLine) System.out.println("|Call Num|Title|Author|Available No. of Copy|");
                System.out.println("|" + callNum + "|" + title +"|" + author + "|"+ available  +"|");
            }else{
                System.out.println("cannot search any results with the input");
            }
        }catch (SQLException e){

        }
    }

    public void searchByTitle(String title){
        title = title.toLowerCase();
        Connection conn = DatabaseManager.getConnection();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT distinct callnum FROM book where LOWER(title) like ? ORDER BY callnum ASC");
            preparedStatement.setString(1, "%"+title+"%");

            ResultSet rs = preparedStatement.executeQuery();

            boolean hasResult = false;
            boolean first = true;
            while (rs.next()){
                if (first){ System.out.println("|Call Num|Title|Author|Available No. of Copy|"); first = false;}
                hasResult = true;
                String callnum = rs.getString("callnum");
                searchByCallNum(callnum, false);
            }
            if (!hasResult) System.out.println("Cannot search any results with the input");
        } catch (SQLException e) {

        }
    }

    public void searchByAuthor(String name){
        name = name.toLowerCase();
        Connection conn = DatabaseManager.getConnection();
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT DISTINCT callnum FROM authorship WHERE LOWER(aname) LIKE ? ORDER BY callnum ASC");
            preparedStatement.setString(1, "%"+name+"%");

            ResultSet rs = preparedStatement.executeQuery();
            boolean hasResult = false;
            boolean first = true;
            while (rs.next()){
                if (first){ System.out.println("|Call Num|Title|Author|Available No. of Copy|"); first = false;}
                hasResult = true;
                String callnum = rs.getString("callnum");
                searchByCallNum(callnum, false);
            }
            if (!hasResult) System.out.println("Cannot search any results with the input");
            /*
            String sql = "SELECT * callnum FROM authorship";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()){
                System.out.println(rs.getString("callnum")+ "\t" + rs.getString("aname"));
            }
            */
        }catch (SQLException e){

        }
    }
    public void showUserRecords() {
        //
        System.out.println("");
        System.out.println("Please enter your user ID:");
        Scanner keyboard = new Scanner(System.in);
        String userID = keyboard.nextLine();
        Connection conn = DatabaseManager.getConnection();
        try{
            PreparedStatement pStmt = conn.prepareStatement("SELECT * FROM BORROW WHERE libuid = ? ORDER BY checkoutdate DESC");
            pStmt.setString(1, userID);

            ResultSet rs = pStmt.executeQuery();
            boolean hasResult = false;
            boolean first = true;
            while (rs.next()){
                if (first){
                    System.out.println("Loan Record:");
                    System.out.println("|CallNum|CopyNum|Title|Author|Check-out|Returned?|");
                }
                first = false;
                hasResult = true;
                String callNum = rs.getString("callnum");
                Date checkOutDate = rs.getDate("checkoutdate");
                String returnDate = rs.getString("returndate");
                String returned = (rs.wasNull()) ? "No" : "Yes";
                String copyNum = rs.getString("copynum");
                String sql = "SELECT * FROM book, authorship where " +  "book.callnum = authorship.callnum and book.callnum = '"+ callNum +"'";
                Statement statement2 = conn.createStatement();
                ResultSet rs2 = statement2.executeQuery(sql);
                rs2.next();
                String author = rs2.getString("aname");
                String title = rs2.getString("title");;
                while(rs2.next()) author += ", "+ rs2.getString("aname");
                System.out.println("|" + callNum + "|" + copyNum + "|" + title  + "|" + author  + "|" + DatabaseManager.dateToString(checkOutDate)  + "|" + returned + "|");
            }
            if (!hasResult) System.out.println("Cannot search any results with the input");
        }catch (SQLException e){

        }
    }
}
