import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by tat on 29/2/2016.
 */
public class MenuManager {
    public void displayMainMenu() {
        System.out.println("");
        System.out.println("-----Main menu-----");
        System.out.println("What kinds of operations would you like to perform?");
        System.out.println("1. Operations for administrator");
        System.out.println("2. Operations for library user");
        System.out.println("3. Operations for librarian");
        System.out.println("4. Operations for library director");
        System.out.println("5. Exit this program");

        // Retrieve user's choice
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter Your Choice: ");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){

        }

        if (choice == 1) {
            displayAdminMenu();
        } else if (choice == 2) {
            displayUserMenu();
        } else if (choice == 3) {
            displayLibrarianMenu();
        } else if (choice == 4) {
            displayDirectorMenu();
        } else if (choice == 5) {
            System.exit(0);
        } else {
            System.err.println("Invalid input, please enter number 1, 2 or 3");
            displayMainMenu();
        }
    }

    public void displayAdminMenu() {
        System.out.println("");
        System.out.println("-----Operations for administrator menu-----");
        System.out.println("What kinds of operation would you like to perform?");
        System.out.println("1. Create all tables");
        System.out.println("2. Delete all tables");
        System.out.println("3. Load from datafile");
        System.out.println("4. Show number of records in each table");
        System.out.println("5. Return to the main menu");

        // Retrieve user's choice
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter Your Choice: ");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){

        }

        AdministratorAction action = new AdministratorAction();
        if (choice == 1) {
            action.createTables();
        } else if (choice == 2) {
            action.deleteTables();
        } else if (choice == 3) {
            action.loadDataFiles();
        } else if (choice == 4) {
            action.countRecords();
        } else if (choice == 5) {
            displayMainMenu();
            return;
        } else {
            System.err.println("Invalid input, please enter number 1, 2 or 3");
        }

        System.out.println("Please enter to continue.");
        try {
            System.in.read();
        } catch (Exception e) {
            //
        }
        displayAdminMenu();
    }

    public void displayUserMenu() {
        System.out.println("");
        System.out.println("-----Operations for library user menu-----");
        System.out.println("What kinds of operation would you like to perform?");
        System.out.println("1. Search for Books");
        System.out.println("2. Show load record of a user");
        System.out.println("3. Return to the main menu");

        // Retrieve user's choice
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter Your Choice: ");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){
        }

        LibraryUserAction action = new LibraryUserAction();
        if (choice == 1) {
            action.searchBooks();
        } else if (choice == 2) {
            action.showUserRecords();
        } else if (choice == 3) {
            displayMainMenu();
            return;
        } else {
            System.err.println("Invalid input, please enter number 1, 2 or 3");
        }

        System.out.println("Please enter to continue.");
        try {
            System.in.read();
        } catch (Exception e) {
        }
        displayUserMenu();
    }

    public void displayLibrarianMenu() {
        System.out.println("");
        System.out.println("-----Operations for librarian menu-----");
        System.out.println("What kinds of operation would you like to perform?");
        System.out.println("1. Book Borrowing");
        System.out.println("2. Book Returning");
        System.out.println("3. Return to the main menu");

        // Retrieve user's choice
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter Your Choice: ");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){

        }

        LibrarianAction action = new LibrarianAction();
        if (choice == 1) {
            action.borrowBook();
        } else if (choice == 2) {
            action.returnBook();
        } else if (choice == 3) {
            displayMainMenu();
            return;
        } else {
            System.err.println("Invalid input, please enter number 1, 2 or 3");
        }

        System.out.println("Please enter to continue.");
        try {
            System.in.read();
        } catch (Exception e) {
            //
        }
        displayLibrarianMenu();
    }

    public void displayDirectorMenu() {
        System.out.println("");
        System.out.println("-----Operations for library director menu-----");
        System.out.println("What kinds of operation would you like to perform?");
        System.out.println("1. List all un-returned book copies which are checked-out within a period");
        System.out.println("2. Return to the main menu");

        // Retrieve user's choice
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter Your Choice: ");
        int choice = 0;
        try{
            choice = keyboard.nextInt();
        }catch (InputMismatchException e){

        }

        DirectorAction action = new DirectorAction();
        if (choice == 1) {
            action.listUnreturned();
        } else if (choice == 2) {
            displayMainMenu();
            return;
        } else {
            System.err.println("Invalid input, please enter number 1, 2 or 3");
        }

        System.out.println("Please enter to continue.");
        try {
            System.in.read();
        } catch (Exception e) {
            //
        }
        displayDirectorMenu();
    }
}
