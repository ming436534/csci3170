import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Po on 3/4/2016.
 */
public class DatabaseManager {
    private static Connection connection;

    public static boolean connect(String dbUsername, String dbPassword) {
        // Connect to the database
        try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@db12.cse.cuhk.edu.hk:1521:db12", dbUsername, dbPassword);
        } catch (Exception e) {
            System.err.println("Failed to connect to the database. Error: " + e.getLocalizedMessage());
            return false;
        }

        return true;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            System.err.println("Failed to close the database connection. Error: " + e.getLocalizedMessage());
        }
    }

    public static Date stringToDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date date = formatter.parse(dateString);
            return new java.sql.Date(date.getTime());
        } catch (ParseException e) {
            //
        }
        return null;
    }

    public static String dateToString(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);
    }
}
