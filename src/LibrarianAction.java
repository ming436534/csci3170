import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.sql.*;
import java.util.Date;

/**
 * Created by Javier Rodriguez on 10/4/2016.
 * Note: The commented section "Printing the current Data Base" is for checking the update of the records.
 */
public class LibrarianAction {
    public void borrowBook() {
        String userId, callNo;
        int copyNo;

        Connection conn = DatabaseManager.getConnection();

        // Ask for the User ID
        System.out.print("Enter The User ID: ");
        Scanner keyboard = new Scanner(System.in);
        userId = keyboard.next();

        if (userId.isEmpty()) {
            System.err.println("Invalid User ID");
            return;
        }

        // Ask for the Call Number
        System.out.print("Enter The Call Number: ");
        callNo = keyboard.next();

        if (callNo.isEmpty()) {
            System.err.println("Invalid Call Number");
            return;
        }

        // Ask for the Copy Number
        System.out.print("Enter The Copy Number: ");
        copyNo = keyboard.nextInt();

        try {
            // Check whether the call number and copy number are fine
            PreparedStatement pStmt = conn.prepareStatement("SELECT * FROM BOOKCOPY WHERE CALLNUM = ? AND COPYNUM = ?");
            pStmt.setString(1, callNo);
            pStmt.setInt(2, copyNo);
            ResultSet rs = pStmt.executeQuery();
            if (!rs.next()) {
                System.out.println("[Error] Invalid call number or copy number");
                return;
            }

            // Getting all the borrow records
            String condition = "SELECT * FROM borrow WHERE callnum = ? AND copynum = ? AND returndate IS NULL";
            pStmt = conn.prepareStatement(condition);
            pStmt.setString(1, callNo);
            pStmt.setInt(2, copyNo);

            rs = pStmt.executeQuery();

            if (rs.next()) {
                System.out.println("[Error] This book is not available to borrow");
                return;
            }

            // Insert record to the database
            String sql = "INSERT INTO borrow (libuid, callnum, copynum, checkoutdate, returndate) VALUES (?, ?, ?, CURRENT_DATE, ?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, userId);
            pstmt.setString(2, callNo);
            pstmt.setInt(3, copyNo);
            pstmt.setDate(4, null);

            pstmt.executeUpdate();
            pstmt.close();

            System.out.println("Book borrowing performed successfully!!!");

            rs.close();

        } catch (Exception e) {
            System.err.println("\nFailed the query. Error: " + e.getLocalizedMessage());
        }
    }

    public void returnBook() {
        String userId, callNo;
        int copyNo;

        Connection conn = DatabaseManager.getConnection();

        // Ask for the User ID
        System.out.print("Enter The User ID: ");
        Scanner keyboard = new Scanner(System.in);
        userId = keyboard.next();

        if (userId.isEmpty()) {
            System.err.println("Invalid User ID");
            return;
        }

        // Ask for the Call Number
        System.out.print("Enter The Call Number: ");
        callNo = keyboard.next();

        if (callNo.isEmpty()) {
            System.err.println("Invalid Call Number");
            return;
        }

        // Ask for the Copy Number
        System.out.print("Enter The Copy Number: ");
        copyNo = keyboard.nextInt();

        try {
            // Getting borrowing records
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM BORROW WHERE LIBUID = ? AND CALLNUM = ? AND COPYNUM = ? AND RETURNDATE IS NULL");
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, callNo);
            preparedStatement.setInt(3, copyNo);

            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                System.out.println("[Error] Unable to find the corresponding borrow record");
                return;
            }

            // Update database record
            String sql = "UPDATE BORROW SET RETURNDATE = CURRENT_DATE WHERE LIBUID = ? AND CALLNUM = ? AND COPYNUM = ? AND RETURNDATE IS NULL";
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, callNo);
            preparedStatement.setInt(3, copyNo);

            preparedStatement.executeUpdate();

            System.out.println("Book returning performed successfully!!!");

            rs.close();
        } catch (Exception e) {
            System.err.println("\nFailed the query. Error: " + e.getLocalizedMessage());
        }
    }
}
