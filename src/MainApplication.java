/**
 * Created by tat on 29/2/2016.
 */

public class MainApplication {
    private final static String dbUsername = "c040";
    private final static String dbPassword = "eigphojo";

    public static void main(String[] args) {
        // Load Oracle database driver
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (Exception e) {
            System.err.println("Failed to load the database driver class");
            return;
        }

        // Connect to the database
        if (!DatabaseManager.connect(dbUsername, dbPassword)) {
            return;
        }

        // Interaction with the user
        System.out.println("Welcome to library inquiry system!");
        MenuManager menuManager = new MenuManager();
        menuManager.displayMainMenu();

        // Close the database connection
        DatabaseManager.closeConnection();
    }
}
