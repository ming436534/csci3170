import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by Po on 3/4/2016.
 */
public class AdministratorAction {
    public AdministratorAction() {
        // Empty constructor
    }

    public void createTables() {
        System.out.print("Processing...");

        Connection conn = DatabaseManager.getConnection();
        try {
            // User Category Table
            Statement statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE category (" +
                    "  cid SMALLINT PRIMARY KEY NOT NULL," +
                    "  maxBooks SMALLINT NOT NULL," +
                    "  loanPeriod SMALLINT NOT NULL" +
                    ")");
            statement.close();

            // User Table
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE libuser (" +
                    "  libuid VARCHAR2(10) PRIMARY KEY NOT NULL," +
                    "  name VARCHAR2(25) NOT NULL," +
                    "  address VARCHAR2(100) NOT NULL," +
                    "  cid SMALLINT NOT NULL," +
                    "  CONSTRAINT fk_UserCategory FOREIGN KEY (cid) REFERENCES category(cid) ON DELETE CASCADE" +
                    ")");
            statement.close();

            // Book Table
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE book (" +
                    "  callnum VARCHAR2(8) PRIMARY KEY NOT NULL," +
                    "  title VARCHAR2(30) NOT NULL," +
                    "  publish DATE NOT NULL" +
                    ")");
            statement.close();

            // Book Copy Table
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE bookcopy (" +
                    "  callnum VARCHAR2(8) NOT NULL," +
                    "  copynum SMALLINT NOT NULL," +
                    "  CONSTRAINT pk_bookCopy PRIMARY KEY (callnum, copynum), " +
                    "  CONSTRAINT fk_bookCallNum FOREIGN KEY (callnum) REFERENCES book(callnum) ON DELETE CASCADE" +
                    ")");
            statement.close();

            // Borrow Record Table
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE borrow (" +
                    "  libuid VARCHAR2(10) NOT NULL," +
                    "  callnum VARCHAR2(8) NOT NULL," +
                    "  copynum SMALLINT NOT NULL," +
                    "  checkoutdate DATE NOT NULL," +
                    "  returndate DATE NULL," +
                    "  CONSTRAINT pk_borrow PRIMARY KEY (libuid, callnum, copynum, checkoutdate), " +
                    "  CONSTRAINT fk_borrowBookCopy FOREIGN KEY (callnum, copynum) REFERENCES bookcopy(callnum, copynum) ON DELETE CASCADE, " +
                    "  CONSTRAINT fk_borrowUser FOREIGN KEY (libuid) REFERENCES libuser(libuid) ON DELETE CASCADE" +
                    ")");
            statement.close();

            // Authorship Table
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE authorship (" +
                    "  callnum VARCHAR2(8) NOT NULL," +
                    "  aname VARCHAR2(25) NOT NULL," +
                    "  CONSTRAINT pk_bookAuthorship PRIMARY KEY (callnum, aname), " +
                    "  CONSTRAINT fk_authorBookCallNum FOREIGN KEY (callnum) REFERENCES book(callnum) ON DELETE CASCADE" +
                    ")");
            statement.close();
        } catch (Exception e) {
            System.err.println("\nFailed to create tables. Error: " + e.getLocalizedMessage());
            return;
        }

        // Notify the user
        System.out.print("Done! Database is initialized!\n");
    }

    public void deleteTables() {
        System.out.print("Processing...");

        Connection conn = DatabaseManager.getConnection();
        try {
            // User Category Table
            Statement statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE category CASCADE CONSTRAINTS");
            statement.close();

            // User Table
            statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE libuser CASCADE CONSTRAINTS");
            statement.close();

            // Book Table
            statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE book CASCADE CONSTRAINTS");
            statement.close();

            // Book Copy Table
            statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE bookcopy CASCADE CONSTRAINTS");
            statement.close();

            // Borrow Record Table
            statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE borrow");
            statement.close();

            // Authorship Table
            statement = conn.createStatement();
            statement.executeUpdate("DROP TABLE authorship");
            statement.close();
        } catch (Exception e) {
            System.err.println("\nFailed to create tables. Error: " + e.getLocalizedMessage());
            return;
        }

        // Notify the user
        System.out.print("Done! Database is removed!\n");
    }

    public void loadDataFiles() {
        Connection conn = DatabaseManager.getConnection();

        // Ask for data source location
        System.out.print("Type in the Source Data Folder Path: ");
        Scanner keyboard = new Scanner(System.in);
        String folderPath = keyboard.next();

        System.out.print("Processing...");

        try {
            // User Category
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO category " +
                    "(cid, maxBooks, loanPeriod) " +
                    "VALUES (?, ?, ?)");

            Scanner in = new Scanner(new FileReader(folderPath + "/category.txt"));
            while(in.hasNextLine()) {
                if (!in.hasNextInt()) {
                    break;
                }
                int cid = in.nextInt();
                int maxBooks = in.nextInt();
                int loanPeriod = in.nextInt();

                //System.out.printf("category - %d, %d, %d\n", cid, maxBooks, loanPeriod);

                pstmt.setInt(1, cid);
                pstmt.setInt(2, maxBooks);
                pstmt.setInt(3, loanPeriod);
                pstmt.executeUpdate();
            }
            pstmt.close();
        } catch (IOException e) {
            System.err.println("\nFailed to load data file. Error: " + e.getLocalizedMessage());
            return;
        } catch (SQLException e) {
            System.err.println("\nFailed to insert data to the database. Error: " + e.getLocalizedMessage());
            return;
        }

        try {
            // Library User
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO libuser " +
                    "(libuid, name, address, cid) " +
                    "VALUES (?, ?, ?, ?)");

            Scanner in = new Scanner(new FileReader(folderPath + "/user.txt"));
            while(in.hasNextLine()) {
                String line = in.nextLine();
                if (line.isEmpty()) {
                    break;
                }

                String[] data = line.split("\t");
                if (data.length != 4) {
                    continue;
                }

                String libUid = data[0];
                String name = data[1];
                String address = data[2];
                int cid = Integer.parseInt(data[3]);

                //System.out.printf("libuser - %s, %s, %s, %d\n", libUid, name, address, cid);

                pstmt.setString(1, libUid);
                pstmt.setString(2, name);
                pstmt.setString(3, address);
                pstmt.setInt(4, cid);
                pstmt.executeUpdate();
            }
            pstmt.close();
        } catch (IOException e) {
            System.err.println("\nFailed to load data file. Error: " + e.getLocalizedMessage());
            return;
        } catch (SQLException e) {
            System.err.println("\nFailed to insert data to the database. Error: " + e.getLocalizedMessage());
            return;
        }

        try {
            // Book
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO book " +
                    "(callnum, title, publish) " +
                    "VALUES (?, ?, ?)");
            PreparedStatement pstmtCopy = conn.prepareStatement("INSERT INTO bookCopy " +
                    "(callnum, copynum) " +
                    "VALUES (?, ?)");
            PreparedStatement pstmtAuthor = conn.prepareStatement("INSERT INTO authorship " +
                    "(callnum, aname) " +
                    "VALUES (?, ?)");

            Scanner in = new Scanner(new FileReader(folderPath + "/book.txt"));
            while(in.hasNextLine()) {
                String line = in.nextLine();
                if (line.isEmpty()) {
                    break;
                }

                String[] data = line.split("\t");
                if (data.length != 5) {
                    continue;
                }

                String callNum = data[0];
                int numOfCopies = Integer.parseInt(data[1]);
                String title = data[2];
                String authors = data[3];
                String publish = data[4];

                //System.out.printf("book - %s, %s, %s\n", callNum, title, publish);

                pstmt.setString(1, callNum);
                pstmt.setString(2, title);
                pstmt.setDate(3, DatabaseManager.stringToDate(publish));
                pstmt.executeUpdate();

                // Book Copies
                for (int i = 1; i <= numOfCopies; i++) {
                    //System.out.printf("bookCopy - %s, %d\n", callNum, i);
                    pstmtCopy.setString(1, callNum);
                    pstmtCopy.setInt(2, i);
                    pstmtCopy.executeUpdate();
                }

                // Authorship
                for (String author : authors.split(",")) {
                    //System.out.printf("authorship - %s, %s\n", callNum, author);
                    pstmtAuthor.setString(1, callNum);
                    pstmtAuthor.setString(2, author);
                    pstmtAuthor.executeUpdate();
                }
            }
            pstmtCopy.close();
            pstmt.close();

        } catch (IOException e) {
            System.err.println("\nFailed to load data file. Error: " + e.getLocalizedMessage());
            return;
        } catch (SQLException e) {
            System.err.println("\nFailed to insert data to the database. Error: " + e.getLocalizedMessage());
            return;
        }

        try {
            // Borrow Record
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO borrow " +
                    "(callNum, copyNum, libUid, checkoutDate, returnDate) " +
                    "VALUES (?, ?, ?, ?, ?)");

            Scanner in = new Scanner(new FileReader(folderPath + "/check_out.txt"));
            while(in.hasNextLine()) {
                String line = in.nextLine();
                if (line.isEmpty()) {
                    break;
                }

                String[] data = line.split("\t");
                if (data.length != 5) {
                    continue;
                }

                String callNum = data[0];
                int copyNum = Integer.parseInt(data[1]);
                String libUid = data[2];
                String checkOutDate = data[3];
                String returnDate = data[4];

                //System.out.printf("borrow - %s, %d, %s, %s, %s\n", callNum, copyNum, libUid, checkOutDate, returnDate);
                pstmt.setString(1, callNum);
                pstmt.setInt(2, copyNum);
                pstmt.setString(3, libUid);
                pstmt.setDate(4, DatabaseManager.stringToDate(checkOutDate));
                if (returnDate.equals("null")) {
                    pstmt.setNull(5, Types.DATE);
                } else {
                    pstmt.setDate(5, DatabaseManager.stringToDate(returnDate));
                }

                pstmt.executeUpdate();
            }
            pstmt.close();
        } catch (IOException e) {
            System.err.println("\nFailed to load data file. Error: " + e.getLocalizedMessage());
            return;
        } catch (SQLException e) {
            System.err.println("\nFailed to insert data to the database. Error: " + e.getLocalizedMessage());
            return;
        }

        // Notify the user
        System.out.print("Done! Data is inputted to the database!\n");
    }

    public void countRecords() {
        System.out.println("Number of records in each table:");

        Connection conn = DatabaseManager.getConnection();
        try {
            String[] tables = {"category", "libuser", "book", "bookcopy", "borrow", "authorship"};
            for (String table : tables) {
                Statement statement = conn.createStatement();
                ResultSet rs = statement.executeQuery(String.format("SELECT COUNT(*) FROM %s", table));
                rs.next();
                System.out.printf("%s: %d\n", table, rs.getInt(1));
                rs.close();
                statement.close();
            }
        } catch (Exception e) {
            System.err.println("\nFailed to create tables. Error: " + e.getLocalizedMessage());
            return;
        }
    }
}
