import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

/**
 * Created by Po on 3/4/2016.
 */
public class DirectorAction {
    public void listUnreturned() {
        Scanner keyboard = new Scanner(System.in);

        // Ask for the date range
        System.out.print("Type in the starting date [dd/mm/yyyy]: ");
        String startDateString = keyboard.next();
        if (startDateString.isEmpty()) {
            System.err.println("Invalid starting date");
            return;
        }
        Date startDate = DatabaseManager.stringToDate(startDateString);
        if (startDate == null) {
            System.err.println("Invalid starting date");
            return;
        }

        System.out.print("Type in the ending date [dd/mm/yyyy]: ");
        String endDateString = keyboard.next();
        if (endDateString.isEmpty()) {
            System.err.println("Invalid ending date");
            return;
        }
        Date endDate = DatabaseManager.stringToDate(endDateString);
        if (endDate == null) {
            System.err.println("Invalid ending date");
            return;
        }

        try {
            // Find records from the database
            Connection conn = DatabaseManager.getConnection();
            PreparedStatement pStmt = conn.prepareStatement("SELECT libuid, callNum, copyNum, checkoutDate FROM BORROW WHERE returnDate IS NULL AND checkoutDate BETWEEN ? AND ?");
            pStmt.setDate(1, startDate);
            pStmt.setDate(2, endDate);

            ResultSet rs = pStmt.executeQuery();

            // Display the result
            System.out.println("List of Un-returned Book:");
            System.out.println("|LibUID|CallNum|CopyNum|Checkout|");
            while (rs.next()) {
                System.out.printf("|%s|%s|%d|%s|\n", rs.getString("libUid"), rs.getString("callNum"), rs.getInt("copyNum"), DatabaseManager.dateToString(rs.getDate("checkoutDate")));
            }


            System.out.println("End of Query");
        } catch (Exception e) {
            System.err.println("Failed to retrieve data from the database. Error: "+e.getLocalizedMessage());
        }
    }
}
